*** Settings ***
Documentation  Basic example for using Robot Framework standard Telnet library 
...            and using commandline options for ignoring test cases.
Library  Telnet

Default Tags    ignore

*** Variables ***
${server}    rainmaker.wunderground.com
${port}      3000


*** Test Cases ***
Example: Connect to weather portal
  Open Connection  ${server}  port=${port}
  Set Prompt  Press Return to continue:
  Write  \r
  Set Prompt  city code--
  Write  \r
  Set Prompt  Selection:
  Write Bare  X\r
  Close Connection

Get New York City weather forecast
  Open Connection  rainmaker.wunderground.com  port=3000  
  Set Prompt  Press Return to continue:
  Read Until Prompt
  Write  \r
  Set Prompt  city code--
  Read Until Prompt
  Write  NYC
  Set Prompt  X to exit:
  ${out}=  Read Until Prompt
  Write Bare  X\r
  Close Connection
  Log To Console  ${out}

Telnet cimt ag (ignore)
  [Tags]  ignore
  Open Connection  cimt-ag.de  23
  Close Connection