*** Settings ***
Documentation  Example for using an external database library,
...            writing a test suite for an ETL job and using variable files.
Library  DatabaseLibrary
Library  OperatingSystem


*** Variables ***
${schema_name}     workshop


*** Test Cases ***
Example
  Connect To Database    dbapiModuleName=psycopg2    dbName=robot    dbUsername=postgres    dbPassword=postgres    dbHost=localhost    dbPort=5432
  Run Keyword And Ignore Error    Execute Sql String    DROP SCHEMA ${schema_name} CASCADE
  Execute Sql String    CREATE SCHEMA ${schema_name}
  ${res}=    Query    SELECT schema_name FROM information_schema.schemata where schema_name = '${schema_name}'
  Log  ${res}[0]
  Should Be Equal    ${schema_name}    ${res}[0][0]
  Disconnect From Database

Database Test
  Log To Console    In this test case you should verify the table content.

Check Variables
  Log To Console    In this test case you should verify that variables are loaded correctly.

Load Data To Stage
  Log To Console    In this test case you should run the Talend job and verify that the job wrote data to the target table.


*** Keywords ***
Prepare Database
  Log To Console    This keyword should setup the database.

Run Talend Job With Parameters Succesfully
  Log To Console    This keyword should run the Talend job and check the return code.