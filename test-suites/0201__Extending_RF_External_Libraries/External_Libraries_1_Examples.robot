*** Settings ***
Documentation  Example for using an external database library,
...            writing a test suite for an ETL job and using variable files.
Library  DatabaseLibrary
Library  OperatingSystem


*** Variables ***
${schema_name}     workshop


*** Test Cases ***
Example
  Connect To Database    dbapiModuleName=psycopg2    dbName=robot    dbUsername=postgres    dbPassword=postgres    dbHost=localhost    dbPort=5432
  Run Keyword And Ignore Error    Execute Sql String    DROP SCHEMA ${schema_name} CASCADE
  Execute Sql String    CREATE SCHEMA ${schema_name}
  ${res}=    Query    SELECT schema_name FROM information_schema.schemata where schema_name = '${schema_name}'
  Log  ${res}[0]
  Should Be Equal    ${schema_name}    ${res}[0][0]
  Disconnect From Database